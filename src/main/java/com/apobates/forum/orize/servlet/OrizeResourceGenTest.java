package com.apobates.forum.orize.servlet;

import com.apobates.forum.orize.*;
import com.apobates.forum.orize.core.*;
import com.apobates.forum.orize.core.plug.OrizeResourceXmlLoader;
import javax.xml.bind.JAXBException;
import java.net.MalformedURLException;
import java.util.*;

/**
 * 生成xml/json文件
 */
public class OrizeResourceGenTest {
    public static void main(String[] args) {
        //生成文件
        //genResourceFile();
        //测试地址匹配
        //Optional<OrizeMatchResult> matchResult = matchResource();
        //System.out.println(matchResult.map(OrizeMatchResult::getMessage).orElse("FAIL"));
        /*扫描注解
        OrizeAnnotationScanner oass = new OrizeAnnotationSpringScanner();
        oass.scan("com.apobates.forum.trident.controller").stream().forEach(res->{
            System.out.println(
                    String.format("{roles:%s, method:%s, action:%s, spot:%s, path:%s",
                            res.getRoles(),
                            res.getMethod(),
                            res.getAction(),
                            res.getSpot(),
                            res.getPath()));

        });*/
        //测试匹配
        //matchTest();
        threadsMatchTest();
    }
    private static void threadsMatchTest(){
        String reqPath="/member/home/contact";
        String slotReqPath="/board/20201.xhtml";
        String srp = "/board/volumes/20201001.xhtml";
        String srpe = "/board/2020/20201001.xhtml";
        String ttt = "/topic/config/edit";
        String notp = "/order/list";
        String b = "/board/2-1.xhtml";
        String t = "/topic/16-3-1.xhtml";

        final OrizeExecutor oe = OrizeExecutorFactory.classical(OrizeClassicalExecutor.xml("c:/home/resources.xml"));
        Arrays.asList(reqPath, slotReqPath,srp,srpe,ttt,notp,b,t).parallelStream().map(er->{
            OrizeExpression oep = new OrizeExpression(){
                @Override
                public String method() {
                    return "GET";
                }

                @Override
                public String path() {
                    return er;
                }

                @Override
                public boolean ajax() {
                    return false;
                }
            };
            try {
                return oe.match(oep);
            } catch (MalformedURLException e) {
                return null;
            }
        }).filter(Objects::nonNull).forEach((mor)->OrizeResourceGenTest.printMatchResult(mor));
    }
    private static void matchTest(){
        String reqMethod="get";
        String reqPath="/member/home/contact";
        String slotReqPath="/board/20201.xhtml";
        String srp = "/board/volumes/20201001.xhtml";
        String srpe = "/board/2020/20201001.xhtml";
        String ttt = "/topic/config/edit";
        String notp = "/order/list";
        String b = "/board/2-1.xhtml";
        String t = "/topic/16-3-1.xhtml";
        Optional<OrizeMatchResult> matchResult = matchResource("c:/home/resources.xml", reqMethod, t);
        if(matchResult.isPresent()){
            OrizeMatchResult r = matchResult.get();
            printMatchResult(r);
        }else{
            System.out.println("FAIL");
        }
    }
    private static void printMatchResult(OrizeMatchResult r){
        System.out.println("----------------------RL----------------------");
        System.out.println("ReqMethod:"+r.getExpression().method());
        System.out.println("ReqPath:"+r.getExpression().path());
        //
        //显示资源定义
        OrizeResource ors = Optional.ofNullable(r.getResource()).orElse(OrizeAuthCommonHelper.notFindResource());
        System.out.println("ResMethod:" + ors.getMethod());
        System.out.println("ResPath:" + ors.getPath());
        //
        System.out.println("msg:"+r.getMessage());
        System.out.println("----------------------RL----------------------");
    }
    private static Optional<OrizeMatchResult> matchResource(String xmlFilePath, final String method, final String reqPath){
        //final String xmlfile="d:\\logs\\resources.xml";
        OrizeExecutor oe = OrizeExecutorFactory.classical(OrizeClassicalExecutor.xml(xmlFilePath));
        OrizeExpression oep = new OrizeExpression(){
            @Override
            public String method() {
                return method;
            }

            @Override
            public String path() {
                return reqPath;
            }

            @Override
            public boolean ajax() {
                return false;
            }
        };
        try {
            return Optional.ofNullable(oe.match(oep));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    private static void genResourceFile(){
        final String xmlfile="d:\\logs\\resources.xml";
        final String jsonfile="d:\\logs\\resources.json";

        OrizeResource o1 = new OrizeResource("/orders/list", OrizeAction.VIEW, "Member");
        OrizeResource o2 = new OrizeResource("/orders/view", OrizeAction.VIEW, "Member");
        OrizeResource o3 = new OrizeResource("/orders/create", OrizeAction.ADD, "Member");
        //String path, String method, String action, String roles, String spot
        OrizeResource o4 = new OrizeResource("/product/edit", "post", "add", "Admin", "action");
        OrizeResource o5 = new OrizeResource("/product/edit", "post", "edit", "Admin", "action");
        //OrizeResourceXmlLoader xmlLoader = new OrizeResourceXmlLoader("d:\\log\\resources.xml");
        OrizeResources rss = new OrizeResources("20210808", Arrays.asList(o1,o2,o3,o4,o5));

        try {
            OrizeResourceXmlLoader.marshal(rss, xmlfile);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        /*
        try {
            OrizeResourceJsonLoader.marshal(rss, jsonfile);
        } catch (JAXBException e) {
            e.printStackTrace();
        }*/
    }
}
