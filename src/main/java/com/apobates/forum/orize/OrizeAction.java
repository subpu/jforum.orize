package com.apobates.forum.orize;

/**
 * 操作枚举
 * @author xiaofanku@live.cn
 * @since 20210822
 */
public enum OrizeAction {
    /**
     * 查看/method=get,action=VIEW
     */
    VIEW(1, "get"),
    /**
     * 删除/method=delete,action=DEL
     */
    DEL(2, "delete"),
    /**
     * 新增/method=post,action=ADD
     */
    ADD(3, "post"),
    /**
     * 编辑/method=put,action=EDIT
     */
    EDIT(4, "put");

    private final int symbol;
    private final String method;

    OrizeAction(int symbol, String method) {
        this.symbol = symbol;
        this.method = method;
    }

    public int getSymbol() {
        return symbol;
    }

    public String getMethod() {
        return method;
    }
}
