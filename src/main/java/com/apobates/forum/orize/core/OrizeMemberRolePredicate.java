package com.apobates.forum.orize.core;

import java.util.function.BiPredicate;

/**
 * 用户角色验证谓词表达式
 * @author xiaofanku@live.cn
 * @since 20210822
 */
@FunctionalInterface
public interface OrizeMemberRolePredicate {
    /**
     * 返回验证OrizeMember的谓词表达式
     *
     * @return 第一个参数: OrizeMember 用户信息, 第二个参数: 资源中定义的角色要求
     */
    BiPredicate<OrizeMember,String[]> getPredicate();
}
