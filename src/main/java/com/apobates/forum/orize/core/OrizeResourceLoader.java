package com.apobates.forum.orize.core;

import org.w3c.dom.Document;

/**
 * 资源加载器/装载器
 * @author xiaofanku@live.cn
 * @since 20210822
 */
@FunctionalInterface
public interface OrizeResourceLoader {
    /**
     * 加载资源
     * @return
     */
    Document load();
}
