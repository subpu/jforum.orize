package com.apobates.forum.orize.core.plug;

import com.apobates.forum.orize.core.OrizeMember;
import com.apobates.forum.orize.core.OrizeMemberRolePredicate;
import java.util.Arrays;
import java.util.function.BiPredicate;

/**
 * 用户角色验证谓词表达式(角色全包含)
 * 适用于用户可以拥有: 多角色
 * 当前实现是角色全包含.例: 资源中定义了要求两个角色:Member,Admin;OrizeMember.roles也需要全包含:Member,Admin
 * @author xiaofanku@live.cn
 * @since 20210822
 */
public class OrizeMemberRoleContainsAllPredicate implements OrizeMemberRolePredicate {

    @Override
    public BiPredicate<OrizeMember, String[]> getPredicate() {
        return (om, resourcesRoles)->{
            boolean checkRoles = Arrays.asList(om.roles()).containsAll(Arrays.asList(resourcesRoles));
            return checkRoles && om.onlined() && null != om.uid();
        };
    }
}
