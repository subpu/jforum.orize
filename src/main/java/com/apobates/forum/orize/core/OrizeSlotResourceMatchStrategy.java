package com.apobates.forum.orize.core;

/**
 * 占位符资源匹配规则
 * @author xiaofanku@live.cn
 * @since 20210822
 */
@FunctionalInterface
public interface OrizeSlotResourceMatchStrategy {
    /**
     * 匹配
     * @param requestPath 请求地址
     * @param resourcePath 资源定义地址
     * @param resourceSlotsKey 资源定义中的占位符信息,多个占位符之间用逗号分隔
     * @return
     */
    boolean match(String requestPath, String resourcePath, String resourceSlotsKey);
}
