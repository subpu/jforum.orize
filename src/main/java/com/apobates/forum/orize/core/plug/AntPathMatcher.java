package com.apobates.forum.orize.core.plug;

import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * PathMatcher implementation for Ant-style path patterns. Examples are provided below.
 *
 * Part of this mapping code has been kindly borrowed from Apache Ant.
 *
 * The mapping matches URLs using the following rules:
 * ? matches one character
 * * matches zero or more characters
 * ** matches zero or more 'directories' in a path
 * Some examples:
 * com/t?st.jsp - matches com/test.jsp but also com/tast.jsp or com/txst.jsp
 * com/*.jsp - matches all .jsp files in the com directory
 * com/**\/test.jsp - matches all test.jsp files underneath the com path
 * org/apache/shiro/**\/*.jsp - matches all .jsp files underneath the org/apache/shiro path
 * org/**\/servlet/bla.jsp - matches org/apache/shiro/servlet/bla.jsp but also org/apache/shiro/testing/servlet/bla.jsp and org/servlet/bla.jsp
 * N.B.: This class was forked from Apache Shiro with modifications.
 *
 * As per the Apache 2.0 license, the original copyright notice and all author and copyright information have remained intact.
 * -----------------------------------------------------------
 * 代码取自: //micronaut.io
 * io.micronaut.core.util.AntPathMatcher
 * io.micronaut.core.util.StringUtils.tokenizeToStringArray
 * maven: io.micronaut:core:1.0.0.RC2
 * -----------------------------------------------------------
 */
public class AntPathMatcher {
    public static final String DEFAULT_PATH_SEPARATOR = "/";
    private String pathSeparator = "/";

    public AntPathMatcher() {
    }

    public void setPathSeparator(String pathSeparator) {
        this.pathSeparator = pathSeparator != null ? pathSeparator : "/";
    }

    public boolean isPattern(String path) {
        return path.indexOf(42) != -1 || path.indexOf(63) != -1;
    }

    public boolean matches(String pattern, String source) {
        return this.doMatch(pattern, source, true);
    }

    protected boolean doMatch(String pattern, String path, boolean fullMatch) {
        if (path.startsWith(this.pathSeparator) != pattern.startsWith(this.pathSeparator)) {
            return false;
        } else {
            String[] pattDirs = tokenizeToStringArray(pattern, this.pathSeparator);
            String[] pathDirs = tokenizeToStringArray(path, this.pathSeparator);
            int pattIdxStart = 0;
            int pattIdxEnd = pattDirs.length - 1;
            int pathIdxStart = 0;

            int pathIdxEnd;
            String patDir;
            for(pathIdxEnd = pathDirs.length - 1; pattIdxStart <= pattIdxEnd && pathIdxStart <= pathIdxEnd; ++pathIdxStart) {
                patDir = pattDirs[pattIdxStart];
                if ("**".equals(patDir)) {
                    break;
                }

                if (!this.matchStrings(patDir, pathDirs[pathIdxStart])) {
                    return false;
                }

                ++pattIdxStart;
            }

            int patIdxTmp;
            if (pathIdxStart > pathIdxEnd) {
                if (pattIdxStart > pattIdxEnd) {
                    return pattern.endsWith(this.pathSeparator) ? path.endsWith(this.pathSeparator) : !path.endsWith(this.pathSeparator);
                } else if (!fullMatch) {
                    return true;
                } else if (pattIdxStart == pattIdxEnd && pattDirs[pattIdxStart].equals("*") && path.endsWith(this.pathSeparator)) {
                    return true;
                } else {
                    for(patIdxTmp = pattIdxStart; patIdxTmp <= pattIdxEnd; ++patIdxTmp) {
                        if (!pattDirs[patIdxTmp].equals("**")) {
                            return false;
                        }
                    }

                    return true;
                }
            } else if (pattIdxStart > pattIdxEnd) {
                return false;
            } else if (!fullMatch && "**".equals(pattDirs[pattIdxStart])) {
                return true;
            } else {
                while(pattIdxStart <= pattIdxEnd && pathIdxStart <= pathIdxEnd) {
                    patDir = pattDirs[pattIdxEnd];
                    if (patDir.equals("**")) {
                        break;
                    }

                    if (!this.matchStrings(patDir, pathDirs[pathIdxEnd])) {
                        return false;
                    }

                    --pattIdxEnd;
                    --pathIdxEnd;
                }

                if (pathIdxStart > pathIdxEnd) {
                    for(patIdxTmp = pattIdxStart; patIdxTmp <= pattIdxEnd; ++patIdxTmp) {
                        if (!pattDirs[patIdxTmp].equals("**")) {
                            return false;
                        }
                    }

                    return true;
                } else {
                    while(pattIdxStart != pattIdxEnd && pathIdxStart <= pathIdxEnd) {
                        patIdxTmp = -1;

                        int patLength;
                        for(patLength = pattIdxStart + 1; patLength <= pattIdxEnd; ++patLength) {
                            if (pattDirs[patLength].equals("**")) {
                                patIdxTmp = patLength;
                                break;
                            }
                        }

                        if (patIdxTmp == pattIdxStart + 1) {
                            ++pattIdxStart;
                        } else {
                            patLength = patIdxTmp - pattIdxStart - 1;
                            int strLength = pathIdxEnd - pathIdxStart + 1;
                            int foundIdx = -1;
                            int i = 0;

                            label140:
                            while(i <= strLength - patLength) {
                                for(int j = 0; j < patLength; ++j) {
                                    String subPat = pattDirs[pattIdxStart + j + 1];
                                    String subStr = pathDirs[pathIdxStart + i + j];
                                    if (!this.matchStrings(subPat, subStr)) {
                                        ++i;
                                        continue label140;
                                    }
                                }

                                foundIdx = pathIdxStart + i;
                                break;
                            }

                            if (foundIdx == -1) {
                                return false;
                            }

                            pattIdxStart = patIdxTmp;
                            pathIdxStart = foundIdx + patLength;
                        }
                    }

                    for(patIdxTmp = pattIdxStart; patIdxTmp <= pattIdxEnd; ++patIdxTmp) {
                        if (!pattDirs[patIdxTmp].equals("**")) {
                            return false;
                        }
                    }

                    return true;
                }
            }
        }
    }

    private boolean matchStrings(String pattern, String str) {
        char[] patArr = pattern.toCharArray();
        char[] strArr = str.toCharArray();
        int patIdxStart = 0;
        int patIdxEnd = patArr.length - 1;
        int strIdxStart = 0;
        int strIdxEnd = strArr.length - 1;
        boolean containsStar = false;
        char[] var11 = patArr;
        int patLength = patArr.length;

        int strLength;
        for(strLength = 0; strLength < patLength; ++strLength) {
            char aPatArr = var11[strLength];
            if (aPatArr == '*') {
                containsStar = true;
                break;
            }
        }

        char ch;
        int patIdxTmp;
        if (!containsStar) {
            if (patIdxEnd != strIdxEnd) {
                return false;
            } else {
                for(patIdxTmp = 0; patIdxTmp <= patIdxEnd; ++patIdxTmp) {
                    ch = patArr[patIdxTmp];
                    if (ch != '?' && ch != strArr[patIdxTmp]) {
                        return false;
                    }
                }

                return true;
            }
        } else if (patIdxEnd == 0) {
            return true;
        } else {
            while((ch = patArr[patIdxStart]) != '*' && strIdxStart <= strIdxEnd) {
                if (ch != '?' && ch != strArr[strIdxStart]) {
                    return false;
                }

                ++patIdxStart;
                ++strIdxStart;
            }

            if (strIdxStart > strIdxEnd) {
                for(patIdxTmp = patIdxStart; patIdxTmp <= patIdxEnd; ++patIdxTmp) {
                    if (patArr[patIdxTmp] != '*') {
                        return false;
                    }
                }

                return true;
            } else {
                while((ch = patArr[patIdxEnd]) != '*' && strIdxStart <= strIdxEnd) {
                    if (ch != '?' && ch != strArr[strIdxEnd]) {
                        return false;
                    }

                    --patIdxEnd;
                    --strIdxEnd;
                }

                if (strIdxStart > strIdxEnd) {
                    for(patIdxTmp = patIdxStart; patIdxTmp <= patIdxEnd; ++patIdxTmp) {
                        if (patArr[patIdxTmp] != '*') {
                            return false;
                        }
                    }

                    return true;
                } else {
                    while(patIdxStart != patIdxEnd && strIdxStart <= strIdxEnd) {
                        patIdxTmp = -1;

                        for(patLength = patIdxStart + 1; patLength <= patIdxEnd; ++patLength) {
                            if (patArr[patLength] == '*') {
                                patIdxTmp = patLength;
                                break;
                            }
                        }

                        if (patIdxTmp == patIdxStart + 1) {
                            ++patIdxStart;
                        } else {
                            patLength = patIdxTmp - patIdxStart - 1;
                            strLength = strIdxEnd - strIdxStart + 1;
                            int foundIdx = -1;
                            int i = 0;

                            label132:
                            while(i <= strLength - patLength) {
                                for(int j = 0; j < patLength; ++j) {
                                    ch = patArr[patIdxStart + j + 1];
                                    if (ch != '?' && ch != strArr[strIdxStart + i + j]) {
                                        ++i;
                                        continue label132;
                                    }
                                }

                                foundIdx = strIdxStart + i;
                                break;
                            }

                            if (foundIdx == -1) {
                                return false;
                            }

                            patIdxStart = patIdxTmp;
                            strIdxStart = foundIdx + patLength;
                        }
                    }

                    for(patIdxTmp = patIdxStart; patIdxTmp <= patIdxEnd; ++patIdxTmp) {
                        if (patArr[patIdxTmp] != '*') {
                            return false;
                        }
                    }

                    return true;
                }
            }
        }
    }

    public String extractPathWithinPattern(String pattern, String path) {
        String[] patternParts = tokenizeToStringArray(pattern, this.pathSeparator);
        String[] pathParts = tokenizeToStringArray(path, this.pathSeparator);
        StringBuilder buffer = new StringBuilder();
        int puts = 0;

        int i;
        for(i = 0; i < patternParts.length; ++i) {
            String patternPart = patternParts[i];
            if ((patternPart.indexOf(42) > -1 || patternPart.indexOf(63) > -1) && pathParts.length >= i + 1) {
                if (puts > 0 || i == 0 && !pattern.startsWith(this.pathSeparator)) {
                    buffer.append(this.pathSeparator);
                }

                buffer.append(pathParts[i]);
                ++puts;
            }
        }

        for(i = patternParts.length; i < pathParts.length; ++i) {
            if (puts > 0 || i > 0) {
                buffer.append(this.pathSeparator);
            }

            buffer.append(pathParts[i]);
        }

        return buffer.toString();
    }

    public static String[] tokenizeToStringArray(String str, String delimiters) {
        return tokenizeToStringArray(str, delimiters, true, true);
    }

    public static String[] tokenizeToStringArray(String str, String delimiters, boolean trimTokens, boolean ignoreEmptyTokens) {
        if (str == null) {
            return null;
        } else {
            StringTokenizer st = new StringTokenizer(str, delimiters);
            ArrayList tokens = new ArrayList();

            while(true) {
                String token;
                do {
                    if (!st.hasMoreTokens()) {
                        return (String[])tokens.toArray(new String[tokens.size()]);
                    }

                    token = st.nextToken();
                    if (trimTokens) {
                        token = token.trim();
                    }
                } while(ignoreEmptyTokens && token.length() <= 0);

                tokens.add(token);
            }
        }
    }
}
