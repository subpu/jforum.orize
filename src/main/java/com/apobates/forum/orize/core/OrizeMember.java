package com.apobates.forum.orize.core;

/**
 * 当前用户的信息
 * @author xiaofanku@live.cn
 * @since 20210822
 */
public interface OrizeMember {
    /**
     * 用户唯一身份标识/UUID/雪花ID
     * @return
     */
    String uid();

    /**
     * 是否在线
     * @return true在线/false非在线
     */
    boolean onlined();

    /**
     * 角色标识,多个用逗号分隔
     * @return
     */
    String[] roles();
}
