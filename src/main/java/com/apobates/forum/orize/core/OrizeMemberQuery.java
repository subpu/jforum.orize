package com.apobates.forum.orize.core;

import javax.servlet.http.HttpServletRequest;

/**
 * 用户信息查询接口
 * @author xiaofanku@live.cn
 * @since 20210822
 */
@FunctionalInterface
public interface OrizeMemberQuery {
    /**
     * 查询
     * @param request Http请求
     * @return
     */
    OrizeMember query(HttpServletRequest request);
}
