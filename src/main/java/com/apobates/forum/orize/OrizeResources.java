package com.apobates.forum.orize;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * 资源集合类
 * @author xiaofanku@live.cn
 * @since 20210822
 */
@XmlRootElement(name = "resources")
@XmlAccessorType(XmlAccessType.FIELD)
public class OrizeResources implements Serializable {
    @XmlAttribute(name = "stamp")
    private String stamp;

    @XmlElement(name = "item")
    private List<OrizeResource> resources;

    public OrizeResources() {
    }

    public OrizeResources(String stamp, Collection<OrizeResource> resources) {
        this.stamp = stamp;
        this.resources = Collections.unmodifiableList(new ArrayList<>(resources));
    }

    public String getStamp() {
        return stamp;
    }

    public void setStamp(String stamp) {
        this.stamp = stamp;
    }

    public List<OrizeResource> getResources() {
        return resources;
    }

    public void setResources(List<OrizeResource> resources) {
        this.resources = resources;
    }
}
