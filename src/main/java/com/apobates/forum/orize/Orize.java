package com.apobates.forum.orize;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 注解.用于收集资源的定义
 * @author xiaofanku@live.cn
 * @since 20210822
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Orize {
    /**
     * 路径中是否存在占位符
     * 例:
     * /{id}.xhtml 存在
     * /edit 不存在
     * @return true存在/false不存在
     */
    boolean slot()default false;

    /**
     * 若存在占位符,占位的名称是什么
     * 例:/{id}.xhtml, slotKeys={"id"}
     * @return
     */
    String[] slotKeys()default {"*"};

    /**
     * 角色定义
     * 名称和格式无强制格式.只要全局唯一即可
     * @return
     */
    String[] roles();

    /**
     * HTTP方法/GET、POST、PUT、DELETE
     * 全小写
     * @return
     */
    String method();

    /**
     * 操作
     * 多数操作是单一的，但也会存在一些复合的方法,例：新增和编辑是同一个方法
     * @return
     */
    OrizeAction[] action()default {OrizeAction.VIEW};

    /**
     * 请求参数中的标识,当action有多个时用于区分谁是谁?
     * 当action为一个元素时无实际作用,默认值为:do
     * @return
     */
    String spot()default "do";

}
