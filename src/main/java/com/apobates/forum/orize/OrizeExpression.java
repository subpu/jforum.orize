package com.apobates.forum.orize;

/**
 * 请求拦截器生成的表达式
 * @author xiaofanku@live.cn
 * @since 20210822
 */
public interface OrizeExpression {
    /**
     * HTTP方法/GET、POST、PUT、DELETE
     * @return
     */
    String method();

    /**
     * 请求地址的全部,包括查询参数
     * 资源的符号/连接的标识符,例:/order/create.
     * @return
     */
    String path();

    /**
     * 是否是ajax请求
     * @return true是/false不是
     */
    boolean ajax();
}
