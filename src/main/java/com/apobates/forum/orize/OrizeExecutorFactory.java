package com.apobates.forum.orize;

import com.apobates.forum.orize.core.OrizeClassicalExecutor;
import com.apobates.forum.orize.core.OrizeResourceLoader;

/**
 * OrizeExecutor的静态工厂
 * @author xiaofanku@live.cn
 * @since 20210822
 */
public class OrizeExecutorFactory {

    /**
     * 使用W3C的Document来作执行器
     * @param loader
     * @return
     */
    public static OrizeExecutor classical(OrizeResourceLoader loader){
        return OrizeClassicalExecutor.getInstance(loader);
    }

    public static OrizeExecutor memory(){
        throw new UnsupportedOperationException("尚未进行实现");
    }
}
